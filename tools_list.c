/*
** tools_list.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 18:35:44 2016 VEROVE Jordan
** Last update Sat Oct 29 12:34:31 2016 VEROVE Jordan
*/

#include <stdlib.h>
#include "chifumi.h"

t_histo		*add_elem(t_histo *my_list, int ia_command, char *user_play,
			  int turn)
{
  t_histo	*new_elem;

  if ((new_elem = malloc(sizeof(t_histo))) == NULL)
    return (NULL);
  new_elem->ia_command = ia_command;
  new_elem->user_play = user_play;
  new_elem->turn = turn;
  new_elem->next = my_list;
  return (new_elem);
}

t_histo		*my_init_list()
{
  t_histo	*my_list;

  my_list = NULL;
  return (my_list);
}
