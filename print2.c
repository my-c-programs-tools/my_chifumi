/*
** print2.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 16:42:14 2016 VEROVE Jordan
** Last update Sat Oct 29 12:34:08 2016 VEROVE Jordan
*/

#include "chifumi.h"
#include "tools.h"

void		print_bad_cmd()
{
  my_putstr("La commande saisie n'est pas une option valide\n\
Veuillez entre une option valide jeune rebel\n\n");
}

void		print_ia_command(int ia_command)
{
  if (ia_command == 1)
    my_putstr("rock");
  else if (ia_command == 2)
    my_putstr("paper");
  else if (ia_command == 3)
    my_putstr("scissors");
  else if (ia_command == 4)
    my_putstr("well");
  else if (ia_command == 5)
    my_putstr("carpet");
}

void		print_histo_details(t_histo list)
{
  my_putstr("\n------------------------------\nROUND ");
  my_put_nbr(list.turn);
  my_putstr(" :\nYou (");
  my_putstr(list.user_play);
  my_putstr(") ");
  my_put_nbr(list.user_pts);
  my_putstr(" : ");
  my_put_nbr(list.ia_pts);
  my_putstr(" (");
  print_ia_command(list.ia_command);
  my_putstr(") Computer\n\n\n");
}

void		print_histo(t_histo *list)
{
  t_histo	*tmp;

  tmp = list;
  while (tmp)
    {
      print_histo_details(*tmp);
      tmp = tmp->next;
    }
}
