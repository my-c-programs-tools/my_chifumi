/*
** check.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:35:08 2016 VEROVE Jordan
** Last update Fri Oct  4 12:53:06 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include <time.h>
#include "tools.h"
#include "chifumi.h"
#include "print.h"

int	check_and_run(int version, int nb_turn)
{
  if (nb_turn <= 0 || (version != 3 && version != 5)
      || (nb_turn % 2) == 0)
    {
      print_usage();
      return (-1);
    }
  else
    run_chifumi(version, nb_turn);
  return (0);
}

int	check_params(int ac, char **av)
{
  int	i;
  int	version;
  int	nb_turn;

  i = 0;
  while (i != ac)
    {
      if ((my_strcmp(av[i], "-v") == 0) && (i + 1 != ac))
	version = my_getnbr(av[i + 1]);
      else if ((my_strcmp(av[i], "-e") == 0) && (i + 1 != ac))
	nb_turn = my_getnbr(av[i + 1]);
      i++;
    }
  if (check_and_run(version, nb_turn) == -1)
    return (-1);
  return (0);
}

int	check_vs_ia_three(char *str, int ia_play)
{
  if ((my_strcmp_case_insensitive("rock", str) == 0 && ia_play == 1)
      || (my_strcmp_case_insensitive("paper", str) == 0 && ia_play == 2)
      || (my_strcmp_case_insensitive("scissors", str) == 0 && ia_play == 3))
    return (0);
  else if ((my_strcmp_case_insensitive("rock", str) == 0 && ia_play == 2)
	   || (my_strcmp_case_insensitive("paper", str) == 0 && ia_play == 3)
	   || (my_strcmp_case_insensitive("scissors", str) == 0 && ia_play == 1))
    return (-1);
  else
    return (1);
}

int	check_vs_ia_five(char *str, int ia_play)
{
  if ((my_strcmp_case_insensitive("rock", str) == 0 && ia_play == 0)
      || (my_strcmp_case_insensitive("paper", str) == 0 && ia_play == 2)
      || (my_strcmp_case_insensitive("scissors", str) == 0 && ia_play == 3)
      || (my_strcmp_case_insensitive("well", str) == 0 && ia_play == 4)
      || (my_strcmp_case_insensitive("carpet", str) == 0 && ia_play == 5))
    return (0);
  else if ((my_strcmp_case_insensitive("rock", str) == 0 && (ia_play == 3 || ia_play == 5))
	   || (my_strcmp_case_insensitive("paper", str) == 0 && (ia_play == 1 || ia_play == 4))
	   || (my_strcmp_case_insensitive("scissors", str) == 0 && (ia_play == 2 || ia_play == 5))
	   || (my_strcmp_case_insensitive("well", str) == 0 && (ia_play == 1 || ia_play == 3))
	   || (my_strcmp_case_insensitive("carpet", str) == 0 && (ia_play == 2 || ia_play == 4)))
    return (1);
  else
    return (-1);
}

int	check_vs_ia(char *str, int version, int *ia_command, t_histo **list,
		    int *turn)
{
  int	res;
  int	ia_play;

  if (my_strcmp_case_insensitive(str, "quit") == 0)
    return (-3);
  srand(time(NULL));
  ia_play = (rand() % version) + 1;
  *ia_command = ia_play;
  *list = add_elem(*list, ia_play, str, *turn);
  if (version == 3)
    {
      if ((res = check_vs_ia_three(str, ia_play)) != 0)
	*turn += 1;
      return (res);
    }
  else
    {
      if ((res = check_vs_ia_five(str, ia_play)) != 0)
      *turn += 1;
      return (res);
    }
  return (-2);
}
