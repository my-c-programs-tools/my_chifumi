/*
** tools.h for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:22:08 2016 VEROVE Jordan
** Last update Fri Oct  4 12:56:12 2019 VEROVE Jordan
*/

#ifndef _TOOLS_H_
# define _TOOLS_H_

#include "chifumi.h"

void	my_putchar(char c);
void	my_putstr(char *str);
int	my_strlen(char *str);
int	my_strcmp(char *s1, char *s2);
int	my_strcmp_case_insensitive(char *s1, char *s2);
char	*read_line();
int	my_getnbr(char *str);
int	my_wordtab_len(char *str);
void	my_put_nbr_tmp(int nb, int check);
void	my_put_nbr(int nb);
void	add_pts_to_histo(int user_pts, int ia_pts, t_histo **list);

#endif /* !_TOOLS_H_ */
