/*
** check.h for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 13:36:46 2016 VEROVE Jordan
** Last update Sat Oct 29 12:26:12 2016 VEROVE Jordan
*/

#ifndef _CHECK_H_
# define _CHECK_H_

#include "chifumi.h"

int	check_params(int ac, char **av);
int	check_and_run(int version, int nb_turn);
int	check_vs_ia(char *str, int version, int *ia_command, t_histo **list,
		    int *turn);
int	check_vs_ia_three(char *str, int ia_play);
int	check_vs_ia_five(char *str, int ia_play);
int	check_if_quit(t_histo *list);

#endif /* !_CHECK_H_ */
