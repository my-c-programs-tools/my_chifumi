##
## Makefile for chifumi in /home/jordan/rendu/chifumi/verove_j
## 
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
## 
## Started on  Fri Oct 28 11:24:36 2016 VEROVE Jordan
## Last update Sat Oct 29 12:32:28 2016 VEROVE Jordan
##

CC =		gcc

CFLAGS =	-Wall -Werror -Wextra

RM =		rm -rf

SRC =		main.c \
		tools.c \
		tools2.c \
		tools_list.c \
		print.c \
		print2.c \
		check.c \
		check2.c \
		chifumi.c

OBJ =		$(SRC:.c=.o)

NAME =		chifumi

$(NAME):	$(OBJ)
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:		$(NAME)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
