/*
** chifumi.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 12:25:09 2016 VEROVE Jordan
** Last update Sat Oct 29 12:30:07 2016 VEROVE Jordan
*/

#include <stdlib.h>
#include "print.h"
#include "tools.h"
#include "check.h"
#include "chifumi.h"

char		*user_play(int version)
{
  char		*play;

  if ((play = read_line()) == NULL)
    return (NULL);
  if (version == 3)
    {
      if (my_strcmp(play, "rock") == 0 || my_strcmp(play, "paper") == 0 ||
	  my_strcmp(play, "scissors") == 0 || my_strcmp(play, "quit") == 0)
	return (play);
    }
  else if (version == 5)
    {
      if (my_strcmp(play, "rock") == 0 || my_strcmp(play, "paper") == 0 ||
	  my_strcmp(play, "scissors") == 0 || my_strcmp(play, "well") == 0
	  || my_strcmp(play, "carpet") == 0 || my_strcmp(play, "quit") == 0)
	return (play);
    }
  return (NULL);
}

int		change_user_score(int result, int score)
{
  if (result == 1)
    return (score + 1);
  return (score);
}

int		change_ia_score(int result, int score)
{
  if (result == -1)
    return (score + 1);
  return (score);
}

int		chifumi(int version, int nb_turn, int turn, t_histo **list)
{
  int		user_pts;
  int		ia_pts;
  int		res;
  int		ia_command;
  char		*play;

  user_pts = 0;
  ia_pts = 0;
  while (!((user_pts == (nb_turn / 2) + 1) || (ia_pts == (nb_turn / 2) + 1)))
    {
      print_your_turn();
      if (!((play = user_play(version)) == NULL))
	{
	  if ((res = check_vs_ia(play, version, &ia_command, list, &turn)) == -3)
	    return (-1);
	  user_pts = change_user_score(res, user_pts);
	  ia_pts = change_ia_score(res, ia_pts);
	  add_pts_to_histo(user_pts, ia_pts, list);
	  print_result_first(turn, user_pts, ia_pts);
	  print_result_info(play, ia_command, res);
	}
      else
	print_bad_cmd();
    }
  return (0);
}

int		run_chifumi(int version, int nb_turn)
{
  int		quit;
  int		ret;
  t_histo	*list;

  list = my_init_list();
  print_welcome(version, nb_turn);
  if ((ret = chifumi(version, nb_turn, 1, &list)) == -1)
    return (0);
  quit = check_if_quit(list);
  while (quit != 1 && quit != 2)
    quit = check_if_quit(list);
  if (quit == 1)
    run_chifumi(version, nb_turn);
  return (0);
}
