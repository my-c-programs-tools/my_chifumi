/*
** print.h for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:23:33 2016 VEROVE Jordan
** Last update Sat Oct 29 12:24:15 2016 VEROVE Jordan
*/

#ifndef _PRINT_H_
# define _PRINT_H_

#include "chifumi.h"

void	print_usage();
void	print_your_turn();
void	print_welcome(int version, int nb_turn);
void	print_result_first(int turn, int user_score, int ia_score);
void	print_result_info(char *user_play, int ia_command, int result);
void	print_bad_cmd();
void	print_histo(t_histo *list);
void	print_histo_details(t_histo list);

#endif /* !_PRINT_H_ */
