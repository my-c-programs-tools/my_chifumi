/*
** print.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:18:32 2016 VEROVE Jordan
** Last update Sat Oct 29 12:29:05 2016 VEROVE Jordan
*/

#include "tools.h"

void	print_usage()
{
  my_putstr("\nInvalid arguments.\n USAGE: ./chifumi -v \"version (3 or 5)\" \
-e\"duration of the game (odd number)\n\n");
}

void	print_your_turn()
{
  my_putstr("A vous de jouer > ");
}

void	print_welcome(int version, int nb_turn)
{
  my_putstr("------------------------------\n\nVous avez préparé la partie \
avec les paramètres suivant:\n\nVERSION : ");
  if (version == 3)
    my_putstr("ROCK - PAPER - SCISSORS\nBEST OF : ");
  else
    my_putstr("ROCK - PAPER - SCISSORS - WELL - CARPET\nBEST OF : ");
  my_put_nbr(nb_turn);
  my_putstr("\n\n------------------------------\n\n");
}

void	print_result_first(int turn, int user_score, int ia_score)
{
  my_putstr("ROUND ");
  my_put_nbr(turn);
  my_putstr(" : --------------------\n\n");
  my_putstr("You ");
  my_put_nbr(user_score);
  my_putstr(" : ");
  my_put_nbr(ia_score);
  my_putstr(" Computer\nDetails:\n");
}

void	print_result_info(char *user_play, int ia_command, int result)
{
  my_putstr("Vous avez joué \"");
  my_putstr(user_play);
  my_putstr("\"\nComputer a joué \"");
  if (ia_command == 1)
    my_putstr("rock\"\n");
  else if (ia_command == 2)
    my_putstr("paper\"\n");
  else if (ia_command == 3)
    my_putstr("scissors\"\n");
  else if (ia_command == 4)
    my_putstr("well\"\n");
  else
    my_putstr("carpet\"\n");
  if (result == 0)
    my_putstr("EGALITE ! Rejouer le tour\n\n");
  else if (result == 1)
    my_putstr("Vous gagner la manche !\n\n");
  else if (result == -1)
    my_putstr("Vous perdez la manche...\n\n");
}
