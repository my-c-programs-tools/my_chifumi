/*
** chifumi.h for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:38:00 2016 VEROVE Jordan
** Last update Sat Oct 29 12:45:05 2016 VEROVE Jordan
*/

#ifndef _CHIFUMI_H_
# define _CHIFUMI_H_

typedef	struct		s_histo
{
  int			ia_command;
  char			*user_play;
  int			turn;
  int			user_pts;
  int			ia_pts;
  struct s_histo	*next;
}			t_histo;

int			chifumi(int version, int nb_turn, int turn,
				t_histo **list);
int			run_chifumi(int version, int nb_turn);
t_histo			*add_elem(t_histo *my_list, int ia_command,
				  char *user_play, int turn);
t_histo			*my_init_list();

#endif /* !_CHIFUMI_H_ */
