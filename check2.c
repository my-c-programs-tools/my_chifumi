/*
** check2.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 17:34:43 2016 VEROVE Jordan
** Last update Fri Oct  4 12:40:25 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include "tools.h"
#include "print.h"
#include "chifumi.h"

int	check_if_quit(t_histo *list)
{
  char	*result;

  my_putstr("Souhaitez vous rejouer ? [oui - non / exit - history]\n>");
  if ((result = read_line()) == NULL)
    return (-1);
  if (my_strcmp(result, "oui") == 0)
    return (1);
  else if (my_strcmp(result, "non") == 0 || my_strcmp(result, "exit") == 0)
    return (2);
  else if (my_strcmp(result, "history") == 0)
    {
      print_histo(list);
      return (0);
    }
  else
    {
      my_putstr("Désolé, cette commande n'est pas reconnue!\n");
      return (0);
    }
  return (0);
}
