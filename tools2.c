/*
** tools2.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:55:03 2016 VEROVE Jordan
** Last update Sat Oct 29 12:25:19 2016 VEROVE Jordan
*/

#include "tools.h"

int		my_getnbr(char *str)
{
  int		i;
  int		nb;
  int		neg;

  i = 0;
  nb = 0;
  neg = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < '0' || str[i] > '9') && str[i] != '+' && str[i] != '-')
	return (((neg % 2) == 0) ? (nb) : ((nb) * -1));
      if (str[i] == '-')
	neg += 1;
      else if (str[i] != '+')
	{
	  nb = nb * 10;
	  nb = nb + str[i] - 48;
	}
      i++;
    }
  if (neg % 2 == 1)
    return (-1 * nb);
  return (nb);
}

void		my_put_nbr_tmp(int nb, int check)
{
  if (nb < 0 && check == 0)
    {
      my_putchar('-');
    }
  if (nb >= 10 || nb <= -10)
    {
      my_put_nbr_tmp(nb / 10, 1);
      my_put_nbr_tmp(nb % 10, 1);
    }
  else if (nb >= 0)
    my_putchar(nb + 48);
  else if (nb < 0)
    my_putchar((-nb * 2) + nb + 48);
}

void		my_put_nbr(int nb)
{
  my_put_nbr_tmp(nb, 0);
}

void		add_pts_to_histo(int user_pts, int ia_pts, t_histo **list)
{
  (*list)->user_pts = user_pts;
  (*list)->ia_pts = ia_pts;
}
