/*
** tools.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:15:43 2016 VEROVE Jordan
** Last update Fri Oct  4 12:57:29 2019 VEROVE Jordan
*/

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>

void		my_putchar(char c)
{
  write(1, &c, 1);
}

int		my_strlen(char *str)
{
  int		i;

  i = 0;
  while(str[i] != 0)
    i++;
  return (i);
}

void		my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

int		my_strcmp(char *s1, char *s2)
{
  int		i;

  i = 0;
  if (my_strlen(s1) < my_strlen(s2))
    return (-1);
  else if (my_strlen(s1) > my_strlen(s2))
    return (1);
  while (s1[i] != 0)
    {
      if (s1[i] < s2[i])
	return (-1);
      else if (s1[i] > s2[i])
	return (1);
      i++;
    }
  return (0);
}

int		my_strcmp_case_insensitive(char *s1, char *s2)
{
  int		i;

  i = 0;
  if (my_strlen(s1) < my_strlen(s2))
    return (-1);
  else if (my_strlen(s1) > my_strlen(s2))
    return (1);
  while (s1[i] != 0)
    {
      if (s1[i] < s2[i] && ((s1[i] + 32) != s2[i]))
	return (-1);
      else if (s1[i] > s2[i] && ((s1[i] - 32) != s2[i]))
	return (1);
      i++;
    }
  return (0);
}

char		*read_line()
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc(sizeof(char) * 51)) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > 1)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  buff[0] = '\0';
  return (buff);
}
