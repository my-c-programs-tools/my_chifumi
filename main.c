/*
** main.c for chifumi in /home/jordan/rendu/chifumi/verove_j
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Fri Oct 28 11:11:02 2016 VEROVE Jordan
** Last update Sat Oct 29 12:35:02 2016 VEROVE Jordan
*/

#include "print.h"
#include "check.h"

int	main(int ac, char **av)
{
  if (ac != 5 && ac != 7)
    {
      print_usage();
      return (-1);
    }
  check_params(ac, av);
  return (0);
}
